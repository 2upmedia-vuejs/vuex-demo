import Vue from 'vue'
import Vuex from 'vuex'
import Logger from 'vuex/logger'

import snippets from '../components/snippets/vuex-module'

Vue.use(Vuex)
Vue.config.debug = true

export default new Vuex.Store({
    strict: true,

    modules: {
        snippets
    },

    middlewares: [
        Logger()
    ]
})
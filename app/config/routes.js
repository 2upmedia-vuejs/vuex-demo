import app from '../components/app.vue'
import error from '../components/error.vue'
import SnippetsList from '../components/snippets/list.vue'
import SnippetsSave from '../components/snippets/save.vue'
import SnippetsView from '../components/snippets/view.vue'
import GatewaysList from '../components/gateways/list.vue'
import GatewaysSave from '../components/gateways/save.vue'

export const redirects = {
    '/': '/snippets'
}

export default {
    '*': {
        name: 'error',
        component: error
    },

    '/': {
        name: 'home',
        component: app
    },

    '/snippets': {
        component: app,
        name: 'snippets',
        subRoutes: {
            '/': {
                component: SnippetsList
            },
            '/create': {
                component: SnippetsSave
            },
            '/:id': {
                component: SnippetsSave,
                name: 'snippetsSave'
            },
            '/:id/code': {
                component: SnippetsView,
                name: 'snippetsView'
            }
        }
    },

    '/gateways': {
        component: app,
        name: 'gateways',
        subRoutes: {
            '/': {
                component: GatewaysList,
                name: 'gatewaysList'
            },
            '/:gateway': {
                component: GatewaysSave,
                name: 'gatewaysView'
            }
        }
    }
}
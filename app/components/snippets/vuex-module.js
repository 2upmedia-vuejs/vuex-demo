import * as mutation from './vuex-mutation-types'

const state = {
    id: undefined,
    name: null,
    price: null,
    currency: 'usd',
    gateway: 'stripe',
    list: []
}

export const defaultState = Object.assign({}, state)

const mutations = {
    [mutation.LOAD_SNIPPET] (state, data) {
        Object.assign(state, data)
    },
    [mutation.LOAD_SNIPPETS] (state, data) {
        state.list = data
    },
    [mutation.LOAD_EMPTY_SNIPPET] (state, data) {
        Object.assign(state, data)
    },
    [mutation.SAVE_SNIPPET] (state, data) {
        Object.assign(state, data)
    },
}

export default {
    state,
    mutations
}
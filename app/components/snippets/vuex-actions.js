import * as mutations from './vuex-mutation-types'
import { defaultState } from './vuex-module'

var snippet1 = { name: 'Snippet #1', id: "1", price: 4.33, currency: 'usd', gateway: 'stripe'}
var snippet2 = { name: 'Snippet #2', id: "2", price: 148.98, currency: 'eur', gateway: 'quaderno'}

export const actions = {
    loadSnippet({ dispatch }, id) {
        // TODO: async load
        switch (id) {
            case '1':
                var snippet = snippet1
                break
            case '2':
                var snippet = snippet2
                break;
        }

        dispatch(mutations.LOAD_SNIPPET, snippet)
    },
    loadAllSnippets({ dispatch }) {

        dispatch(mutations.LOAD_SNIPPETS, [snippet1, snippet2])
    },
    loadEmptySnippet({ dispatch }) {
        dispatch(mutations.LOAD_EMPTY_SNIPPET, defaultState)
    },
    saveSnippet({ dispatch }, snippet) {
        dispatch(mutations.SAVE_SNIPPET, snippet)
    }
}

export function errorTransitionShake() {
    this.$set('errorTransition', '')

    setTimeout(() => this.$set('errorTransition', 'shake'), 10)
}
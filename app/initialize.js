import Vue from 'vue'
import 'babel-polyfill'
import store from './state/store'
import VueRouter from 'vue-router'
import routes, { redirects } from './config/routes'
import VueResource from 'vue-resource'
import VueValidator from 'vue-validator'

Vue.use(VueRouter)
Vue.use(VueResource)
Vue.use(VueValidator)

const environment = process.env.NODE_ENV

Vue.config.debug = (environment === 'development')
Vue.config.devtools = (environment === 'development')

let application = {
    store
}

let router = new VueRouter({
    history: true
})

router.map(routes)

router.redirect(redirects)

router.start(application, '#content', function() {
    window.App = router.app
})
